import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Main from "./containers/Main/Main";

const App = () => {
	return (
		<Layout>
			<Switch>
				<Route path='/' exact component={Main}/>
				<Route path='/register' component={Register}/>
				<Route path='/login' component={Login}/>
			</Switch>
		</Layout>
	);
};

export default App;
