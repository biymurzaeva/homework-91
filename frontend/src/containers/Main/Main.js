import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {userStates} from "../../store/actions/usersActions";
import Register from "../Register/Register";
import {createMessage, fetchMessages} from "../../store/actions/messagesActions";
import AppDrawer from "../../components/UI/AppDrawer/AppDrawer";
import MessageForm from "../../components/MessageForm/MessageForm";

const useStyles = makeStyles({
	content: {
		flexGrow: 1,
	},
	messagesBlock: {
		height: '345px',
		overflowY: 'scroll',
	},
	message: {
		padding: '10px'
	},
});

const Main = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const ws = useRef(null);
	const user = useSelector(state => state.users.user);
	const logout = useSelector(state => state.users.logout);
	const [users, setUsers] = useState([]);
	const [messages, setMessages] = useState([]);
	const error = useSelector(state => state.messages.createMessageError);
	const historyMessages = useSelector(state => state.messages.messages);
	const loading = useSelector(state => state.messages.fetchLoading);

	useEffect(() => {
		if (user) {
			ws.current = new WebSocket(`ws://localhost:8000/chat?token=${user.token}`);

			setMessages([]);

			ws.current.onclose = () => {
				const intervalId = setInterval(() => {
					ws.current = new WebSocket(`ws://localhost:8000/chat?token=${user.token}`);
				}, 10000);

				clearInterval(intervalId);
			};

			ws.current.onmessage = event => {
				const decoded = JSON.parse(event.data);

				if (decoded.type === 'CONNECTED') {
					setUsers(decoded.onlineUsers);
					dispatch(fetchMessages());
				}

				if (decoded.type === 'NEW_USERS') {
					setUsers(decoded.onlineUsers);
				}

				if (decoded.type === 'NEW_MESSAGE') {
					setMessages(prevState => [...prevState, decoded.message]);
				}
			};
		}

		if (logout) {
			ws.current.send(JSON.stringify({
				type: 'DISCONNECTED',
			}));

			dispatch(userStates());
		}

	}, [dispatch, user, logout]);

	const sendMessage = message => {
		dispatch(createMessage({message: message}));

		ws.current.send(JSON.stringify({
			type: 'CREATE_MESSAGE',
			message,
		}));
	};

	return user ?  (
		<Grid container >
			<AppDrawer users={users}/>
			<Grid item className={classes.content}>
				<Typography component="h1" variant="h6">Chat</Typography>
				<Grid item className={classes.messagesBlock}>
					{loading ? (
						<Grid container justifyContent="center" alignItems="center">
							<Grid item>
								<CircularProgress/>
							</Grid>
						</Grid>
					) : historyMessages.map(message => (
						<Paper elevation={3} key={message._id}>
							<p className={classes.message}><b>{message.user.username}</b>: {message.message}</p>
						</Paper>
					))}

					{messages.map(message => (
						<Paper elevation={3} key={message.datetime}>
							<p className={classes.message}><b>{message.user}: </b>{message.text}</p>
						</Paper>
					))}
				</Grid>
				<MessageForm submitForm={sendMessage} error={error}/>
			</Grid>
		</Grid>
	) : <Register to='/register'/>;
};

export default Main;