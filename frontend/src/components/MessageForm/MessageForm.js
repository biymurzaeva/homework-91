import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
	form: {
		marginTop: theme.spacing(3),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const MessageForm = ({submitForm, error}) => {
	const classes = useStyles();
	const [message, setMessage] = useState('');

	const sendMessage = e => {
		e.preventDefault();
		submitForm(message);
		setMessage('');
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return (
		<Grid item
      component="form"
      container
      className={classes.form}
      onSubmit={sendMessage}
      spacing={2}
		>
			<TextField
				type="text"
				label="Message"
				name="message"
				value={message}
				onChange={e => setMessage(e.target.value)}
				error={Boolean(getFieldError('message'))}
				helperText={getFieldError('message')}
			/>
			<Grid item xs={12}>
				<Button
					type="submit"
					fullWidth
					variant="contained"
					color="primary"
					className={classes.submit}
				>
					Send
				</Button>
			</Grid>
		</Grid>
	);
};

export default MessageForm;