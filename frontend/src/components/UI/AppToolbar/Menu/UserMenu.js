import React from 'react';
import {useDispatch} from "react-redux";
import {Button} from "@material-ui/core";
import {logoutUser} from "../../../../store/actions/usersActions";

const UserMenu = ({user}) => {
	const dispatch = useDispatch();

  return (
    <>
      <Button color="inherit">
        Hello, {user.username}!
      </Button>
	    <Button color="inherit" onClick={() => dispatch(logoutUser())}>Logout</Button>
    </>
  );
};

export default UserMenu;