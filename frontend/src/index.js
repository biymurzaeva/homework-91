import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";
import {Provider} from "react-redux";
import {ToastContainer} from "react-toastify";
import {MuiThemeProvider} from "@material-ui/core";
import App from './App';
import theme from "./theme";
import 'react-toastify/dist/ReactToastify.css';
import history from "./history";
import store from "./store/configureStore";

const app = (
	<Provider store={store}>
		<Router history={history}>
			<MuiThemeProvider theme={theme}>
				<ToastContainer/>
				<App/>
			</MuiThemeProvider>
		</Router>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
