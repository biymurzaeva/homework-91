import {createTheme} from "@material-ui/core";

const theme = createTheme({
	props: {
		MuiTextField: {
			variant: "outlined",
			fullWidth: true,
		},
		body: {
			height: "100%",
			margin: 0,
		},
	},
});

export default theme;