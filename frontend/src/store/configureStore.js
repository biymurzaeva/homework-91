import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import usersReducer from "./reducers/usersReducer";
import axiosApi from "../axiosApi";
import messagesReducer from "./reducers/messagesReducer";

const rootReducer = combineReducers({
	users: usersReducer,
	messages: messagesReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
	rootReducer,
	composeEnhancers(
		applyMiddleware(thunk)
	));

axiosApi.interceptors.request.use(config => {
	try {
		config.headers['Authorization'] = store.getState().users.user.token
	} catch (e) {}

	return config;
});

axiosApi.interceptors.response.use(res => res, e => {
	if (!e.response) {
		e.response = {data: {global: 'No Internet'}};
	}

	throw e;
});

export default store;