import axiosApi from "../../axiosApi";

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_FAILURE = 'CREATE_MESSAGE_FAILURE';

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const createMessageRequest = () => ({type: CREATE_MESSAGE_REQUEST});
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});
export const createMessageFailure = error => ({type: CREATE_MESSAGE_FAILURE, payload: error});

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = error => ({type: FETCH_MESSAGES_FAILURE, payload: error});

export const createMessage = messageData => {
	return async dispatch => {
		try {
			dispatch(createMessageRequest());
			await axiosApi.post('/messages', messageData);
			dispatch(createMessageSuccess());
		} catch (error) {
			dispatch(createMessageFailure(error.response.data));
		}
	};
};

export const fetchMessages = () => {
	return async dispatch => {
		try {
			dispatch(fetchMessagesRequest());
			const response = await axiosApi.get('/messages');
			dispatch(fetchMessagesSuccess(response.data));
		} catch (error) {
			dispatch(fetchMessagesFailure(error));
		}
	};
};

