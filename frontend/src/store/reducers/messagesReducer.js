import {
	CREATE_MESSAGE_FAILURE,
	CREATE_MESSAGE_REQUEST,
	CREATE_MESSAGE_SUCCESS,
	FETCH_MESSAGES_FAILURE,
	FETCH_MESSAGES_REQUEST,
	FETCH_MESSAGES_SUCCESS
} from "../actions/messagesActions";

const initialState = {
	fetchLoading: false,
	fetchError: null,
	messages: [],
	createMessageLoading: false,
	createMessageError: null,
};

const messagesReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_MESSAGES_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_MESSAGES_SUCCESS:
			return {...state, fetchLoading: false, fetchError: null, messages: action.payload};
		case FETCH_MESSAGES_FAILURE:
			return {...state, fetchLoading: false, fetchError: action.payload};
		case CREATE_MESSAGE_REQUEST:
			return {...state, createMessageLoading: true};
		case CREATE_MESSAGE_SUCCESS:
			return {...state, createMessageLoading: false, createMessageError: null};
		case CREATE_MESSAGE_FAILURE:
			return {...state, createMessageLoading: false, createMessageError: action.payload};
		default:
			return state;
	}
};

export default messagesReducer;
