import {
	CLEAR_ERRORS,
	LOGIN_USER_FAILURE, LOGIN_USER_REQUEST,
	LOGIN_USER_SUCCESS, LOGOUT_USER,
	REGISTER_USER_FAILURE, REGISTER_USER_REQUEST,
	REGISTER_USER_SUCCESS, USER_STATES
} from "../actions/usersActions";

export const initialState = {
  registerError: null,
  loginError: null,
  loginLoading: false,
	registerLoading: false,
  user: null,
	logout: null,
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
	  case REGISTER_USER_REQUEST:
	  	return {...state, registerLoading: true}
    case REGISTER_USER_SUCCESS:
      return {...state, registerLoading: false, user: action.payload, registerError: null};
    case REGISTER_USER_FAILURE:
      return {...state, registerLoading: false, registerError: action.payload};
    case LOGIN_USER_REQUEST:
      return {...state, loginLoading: true}
    case LOGIN_USER_SUCCESS:
      return {...state, loginError: null, loginLoading: false, user: action.payload};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.payload, loginLoading: false};
    case LOGOUT_USER:
      return {...state, user: null, logout: true};
	  case CLEAR_ERRORS:
	  	return {...state, loginError: null, registerError: null};
	  case USER_STATES:
	  	return {...state, logout: null};
    default:
      return state;
  }
};

export default usersReducer;