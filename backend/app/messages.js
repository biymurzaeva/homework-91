const express = require('express');
const Message = require('../models/Message');
const auth = require("../middleware/auth");

const router = express.Router();

router.get('/', auth, async (req, res) => {
	try {
		const messages = await Message.find().populate('user', 'username').sort('datetime');

		res.send(messages.slice(Math.max(messages.length - 30, 0)));
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, async (req, res) => {
	try {
		const messageData = {
			message: req.body.message,
			user: req.user._id,
			datetime: new Date().toISOString(),
		};

		const message = new Message(messageData);
		await message.save();
		res.send(message);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	const message = await Message.findById(req.params.id);

	if (!message) {
		return res.status(401).send({error: 'Message not found'});
	}

	if (req.user._id.toString() !== message.user._id.toString()) {
		return res.status(403).send({error: 'Permissions denied!'});
	}

	try {
		await Message.findByIdAndRemove(req.params.id);
		res.send('Message removed');
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;