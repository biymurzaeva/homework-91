const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator')

const MessageSchema = new mongoose.Schema({
	message: {
		type: String,
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	datetime: String,
});

MessageSchema.plugin(idvalidator);
const Message = mongoose.model('Message', MessageSchema);
module.exports = Message;