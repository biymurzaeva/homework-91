const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const User = require("./models/User");
const messages = require("./app/messages");

const app = express();

require('express-ws')(app);

app.use(express.json());
app.use(cors());

const activeConnections = {};
const onlineUsers = [];
const sendMessage = (type, message, users) => {
	Object.keys(activeConnections).forEach(key => {
		const connection = activeConnections[key];
		connection.send(JSON.stringify({
			type: type,
			onlineUsers: users,
			message: message,
		}));
	});
};

const port = 8000;

app.ws('/chat', async (ws, req) => {
	const token = req.query.token;

	const user = await User.findOne({token});

	if (!user) {
		return console.log('User not found');
	}

	activeConnections[user._id] = ws;

	if (!onlineUsers.includes(user.username)) {
		onlineUsers.push(user.username);
	}

	sendMessage('NEW_USERS', null, onlineUsers);

	console.log(`Client connected! id=${user._id}`);

	ws.send(JSON.stringify({type: 'CONNECTED', onlineUsers}))

	ws.on('message', msg => {
		const decoded = JSON.parse(msg);

		switch (decoded.type) {
			case 'CREATE_MESSAGE':

				if (decoded.message !== '') {
					sendMessage('NEW_MESSAGE', {
						user: user.username,
						text: decoded.message,
						datetime: new Date().toISOString()}, null
					);
				}

				break;

			case 'DISCONNECTED':
				delete activeConnections[user._id];
				const deleteUser = onlineUsers.filter(u => u !== user.username);
				sendMessage('NEW_USERS', null, deleteUser);
				console.log(`Client disconnected! id=${user._id}`);
				break;
			default:
				console.log('Unknown type:', decoded.type);
		}
	});

	ws.on('close', () => {
		delete activeConnections[user._id];
		const deleteUser = onlineUsers.filter(u => u !== user.username);
		sendMessage('NEW_USERS', null, deleteUser);
		console.log(`Client disconnected! id=${user._id}`);
	});
});

app.use('/users', users);
app.use('/messages', messages);

const run = async () => {
	await mongoose.connect(config.db.url);

	app.listen(port, () => {
		console.log(`Server started on ${port} port!`);
	});

	exitHook(() => {
		console.log('exiting');
		mongoose.disconnect();
	});
};

run().catch(e => console.error(e));