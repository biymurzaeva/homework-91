const rootPath = __dirname;

module.exports = {
	rootPath,
	db: {
		url: 'mongodb://localhost/chat-lab',
	},
};